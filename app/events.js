import moment from 'moment';

var rightNow = document.createElement('p');
rightNow.textContent = moment().format('MMMM Do YYYY, h:mm:ss a');

document.body.appendChild(rightNow);
