var path = require('path');
const webpack = require('webpack');

module.exports = {
  context: path.resolve(__dirname, './app'),
  entry: {
    app: './index.js',
    events: './events.js',
    vendor: './vendor.js'
  },
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/assets'
  },
  devServer: {
    contentBase: path.resolve(__dirname, '.')
  },
  plugins: [
    new webpack.optimize.CommonsChunkPlugin({
      name: 'commons',
      filename: 'commons.js',
      minChunks: 2
    })
  ]
};
